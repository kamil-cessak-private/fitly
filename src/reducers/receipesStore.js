import {createSlice} from '@reduxjs/toolkit';
import uuid from 'react-native-uuid';

export const receipesStore = createSlice({
  name: 'receipes',
  initialState: {
    receipes: [],
  },
  reducers: {
    addReceipe: (state, action) => {
      let temp = [...state.receipes];
      let data = {...action.payload};
      temp.push({
        id: uuid.v4(),
        title: data.name,
        desc: data.description,
        products: data.selectedProducts,
      });
      state.receipes = temp;
    },
  },
});

export const {addReceipe} = receipesStore.actions;

export default receipesStore.reducer;

export const receipes = state => state.receipes.receipes;
