import {createSlice} from '@reduxjs/toolkit';

export const trainingStore = createSlice({
  name: 'training',
  initialState: {
    training: [],
  },
  reducers: {
    addTraining: (state, action) => {
      let temp = state.training;
      temp.push(action.payload);
      state.training = temp;
    },
  },
});

export const {addTraining} = trainingStore.actions;

export default trainingStore.reducer;

export const training = state => state.training.training;
