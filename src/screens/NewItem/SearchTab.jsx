import React, {useEffect, useState} from 'react';
import {View, Text, ScrollView, TouchableWithoutFeedback} from 'react-native';

import styled from 'styled-components';

import Add from '@images/icons/add.svg';

const SearchItem = ({type, kcal, mealType}) => {
  return (
    <View style={{marginVertical: 8}}>
      {type === 'yesterday' && <RecentMeal>To samo co wczoraj ?</RecentMeal>}
      <TouchableWithoutFeedback>
        <ItemWrapper>
          <Text style={{fontSize: 17}}>{mealType}</Text>
          <RightContainer>
            <KcalWrapper>{kcal + 'kcal'}</KcalWrapper>
            <AddComponent>
              <Add width={30} height={30} fill="#00568a" />
            </AddComponent>
          </RightContainer>
        </ItemWrapper>
      </TouchableWithoutFeedback>
    </View>
  );
};

export const SearchTab = ({data}) => {
  const [yesterday, setYesterday] = useState();
  const [recent, setRecent] = useState([]);

  useEffect(() => {
    if (data && data.length > 0) {
      data.map(e => {
        switch (e.type) {
          case 'yesterday':
            setYesterday(e);
            break;
          default:
            let temp = recent;
            temp.push(e);
            setRecent(temp);
            break;
        }
      });
    }
  }, []);

  const renderRecentSearches = () => {
    if (recent.length > 0) {
      return (
        <View>
          <LastResults>{'Ostatnie wyszukiwania'}</LastResults>
          {recent.map((e, i) => {
            return (
              <SearchItem
                key={i}
                type={e.type}
                mealType={e.mealType}
                kcal={e.kcal}
                desc={e.desc}
              />
            );
          })}
        </View>
      );
    }
  };

  return (
    <ScrollView>
      <View style={{marginTop: 10}}>
        <View>
          {yesterday && Object.keys(yesterday).length > 0 && (
            <SearchItem
              type={yesterday?.type}
              mealType={yesterday?.mealType}
              kcal={yesterday?.kcal}
              desc={yesterday?.desc}
            />
          )}
        </View>
        {renderRecentSearches()}
      </View>
    </ScrollView>
  );
};

const LastResults = styled.Text`
  margin-top: 20px;
  font-size: 15px;
  margin-left: 30px;
  color: #a1a1ab;
`;

const RecentMeal = styled.Text`
  font-size: 15px;
  margin-left: 30px;
  margin-bottom: 5px;
  color: #a1a1ab;
`;

const ItemWrapper = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  background-color: white;
  padding: 20px;
`;

const RightContainer = styled.View`
  flex-direction: row;
  align-items: center;
`;

const KcalWrapper = styled.Text`
  margin-right: 20px;
  font-size: 17px;
  color: #9fb1f1;
`;

const AddComponent = styled.TouchableOpacity`
  background-color: #eaedfc;
  border-radius: 20px;
  padding: 3px;
`;
