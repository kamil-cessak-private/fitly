import React, {useState} from 'react';
import {View, ScrollView, Alert} from 'react-native';
import {useDispatch} from 'react-redux';
import {useNavigation} from '@react-navigation/native';

import styled from 'styled-components';

import {addProduct} from '@reducers/productsStore';

import {ProductInput, SubmitButton} from './Components';

export const ProductTab = () => {
  const [title, set_title] = useState('');
  const [energy, set_energy] = useState();
  const [fats, set_fats] = useState();
  const [protein, set_protein] = useState();
  const [carbons, set_carbons] = useState();
  const [sugar, set_sugar] = useState();

  const nav = useNavigation();
  const dispatch = useDispatch();

  const addValidation = () => {
    if (title !== '' && energy && fats && protein && carbons && sugar) {
      dispatch(
        addProduct({
          title,
          energy,
          fats,
          protein,
          carbons,
          sugar,
        }),
      );
      nav.reset({index: 0, routes: [{name: 'HomeNavigation'}]});
    } else {
      Alert.alert('Brak danych', 'Wprowadź wszystkie dane', [{text: 'OK'}]);
    }
  };
  return (
    <ScrollView
      scrollEnabled={false}
      contentContainerStyle={{flex: 1, justifyContent: 'space-between'}}>
      <View style={{marginTop: 20}}>
        <ProductInput
          title="Nazwa produktu"
          placeholder="Wprowadź nazwę"
          onChange={set_title}
          kbdt="default"
        />
        <TripleInputsWrapper>
          <ProductInput
            title="Energia"
            placeholder="0 kcal"
            onChange={set_energy}
            kbdt="decimal-pad"
          />
          <ProductInput
            title="Tłuszcze"
            placeholder="0 g"
            onChange={set_fats}
            kbdt="decimal-pad"
          />
          <ProductInput
            title="Białko"
            placeholder="0 g"
            onChange={set_protein}
            kbdt="decimal-pad"
          />
        </TripleInputsWrapper>
        <DoubleInputWrapper>
          <ProductInput
            title="Węglowodany"
            placeholder="0 g"
            onChange={set_carbons}
            kbdt="decimal-pad"
          />
          <ProductInput
            title="Cukry"
            placeholder="0 g"
            onChange={set_sugar}
            kbdt="decimal-pad"
          />
        </DoubleInputWrapper>
      </View>
      <SubmitButton validation={addValidation} title="Dodaj produkt" />
    </ScrollView>
  );
};

const TripleInputsWrapper = styled.View`
  flex-direction: row;
  margin-horizontal: 35px;
`;

const DoubleInputWrapper = styled.View`
  flex-direction: row;
  margin-horizontal: 35px;
`;
