import React, {useState} from 'react';
import {
  View,
  Modal,
  Text,
  TouchableOpacity,
  ScrollView,
  Alert,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';

import styled from 'styled-components';

import {useSelector, useDispatch} from 'react-redux';

import Close from '@images/icons/close.svg';
import Done from '@images/icons/done.svg';
import Edit from '@images/icons/edit.svg';

import {products} from '@reducers/productsStore';
import {addReceipe} from '@reducers/receipesStore';

import {ProductInput, SubmitButton} from './Components';

export const ReceipesTab = props => {
  const [modalVisible, set__modalVisible] = useState(false);
  const [usedProducts, set__usedProducts] = useState(useSelector(products));
  const [selectedProducts, set__selectedProducts] = useState([]);
  const [name, set__name] = useState('');
  const [description, set__description] = useState('');

  const nav = useNavigation();
  const dispatch = useDispatch();

  const addOrRemoveItem = (itemID, name) => {
    let temp = [...selectedProducts];
    const found = temp.some(e => e.id === itemID);

    if (!found) {
      temp.push({id: itemID, title: name});
      set__selectedProducts(temp);
    } else {
      const idx = temp.findIndex(e => {
        return e.id === itemID;
      });

      temp.splice(idx, 1);
      set__selectedProducts(temp);
    }
  };

  const renderIcon = index => {
    const found = selectedProducts.some(e => e.id === index);

    if (found) {
      return <Done width={25} height={25} fill="black" />;
    }
    return null;
  };

  const addValidation = () => {
    if (name !== '' && description !== '' && selectedProducts.length > 0) {
      dispatch(
        addReceipe({
          name,
          description,
          selectedProducts,
        }),
      );
      // nav.reset({index: 0, routes: [{name: 'HomeNavigation'}]});
    } else {
      Alert.alert('Brak danych', 'Wprowadź wszystkie dane', [{text: 'OK'}]);
    }
  };

  const inputTextStyles = {marginLeft: 20};

  return (
    <ScrollView style={{opacity: modalVisible ? 0.2 : 1}}>
      <Modal visible={modalVisible} animationType="slide" transparent>
        <ModalWrapper
          style={{
            shadowOffset: {
              width: 0,
              height: 10,
            },
          }}>
          <ModalSubWrapper>
            <NextModalSubWrapper>
              <ModalTextWrapper>
                <Text>Wybierz produkt</Text>
              </ModalTextWrapper>
              <CloseModalIcon
                onPress={() => {
                  set__modalVisible(!modalVisible);
                  props.setOpacity(1);
                }}>
                <Close width={30} height={30} fill="black" />
              </CloseModalIcon>
            </NextModalSubWrapper>
            <ScrollView>
              {usedProducts.map(e => {
                return (
                  <ProductItemWrapper id={e.id} key={e.id}>
                    <Text>{e.title}</Text>
                    <TouchableOpacity
                      id={e.id}
                      onPress={() => addOrRemoveItem(e.id, e.title)}>
                      <IconWrapper>{renderIcon(e.id)}</IconWrapper>
                    </TouchableOpacity>
                  </ProductItemWrapper>
                );
              })}
            </ScrollView>
          </ModalSubWrapper>
        </ModalWrapper>
      </Modal>
      <ProductInput
        title="Nazwa potrawy"
        onChange={set__name}
        kbdt="default"
        placeholder="Wprowadź nazwę"
        textStyle={inputTextStyles}
      />
      <ProductInput
        title="Opis przyrządzenia potrawy"
        onChange={set__description}
        kbdt="default"
        placeholder="Wprowadź opis"
        textStyle={inputTextStyles}
        multiline
        inputStyle={{height: 200}}
      />
      <View>
        <IngredientsWrapper>
          <Text>Składniki potrawy</Text>
          <TouchableOpacity
            onPress={() => {
              set__modalVisible(!modalVisible);
              props.setOpacity(0.2);
            }}>
            <Edit width={20} height={20} fill="black" />
          </TouchableOpacity>
        </IngredientsWrapper>
        <IngredientsList>
          {selectedProducts.length > 0 ? (
            selectedProducts.map((e, i) => {
              return (
                <IngredientsItem key={i}>
                  <Text>{e.title}</Text>
                </IngredientsItem>
              );
            })
          ) : (
            <NoProductsAdded>
              <Text>Dodaj jakiś produkt do przepisu</Text>
            </NoProductsAdded>
          )}
        </IngredientsList>
      </View>
      <SubmitButton
        validation={addValidation}
        title="Dodaj przepis"
        containerStyle={{marginTop: 20}}
      />
    </ScrollView>
  );
};

const ModalWrapper = styled.View`
  flex: 1;
  justify-content: flex-end;
  align-items: center;
  shadow-color: #000;
  shadow-opacity: 0.51;
  shadow-radius: 13.16px;
  elevation: 20;
`;

const ModalSubWrapper = styled.View`
  background-color: white;
  width: 100%;
  height: 75%;
  border-top-left-radius: 40px;
  border-top-right-radius: 40px;
`;

const NextModalSubWrapper = styled.View`
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  padding: 30px;
`;

const ModalTextWrapper = styled.View`
  flex: 1;
  align-items: center;
`;

const CloseModalIcon = styled.TouchableOpacity`
  position: absolute;
  right: 0;
  padding: 10px;
`;

const ProductItemWrapper = styled.TouchableOpacity`
  border-top-width: ${e => (e.id === 0 ? 1 + 'px' : 0 + 'px')};
  border-bottom-width: 1px;
  border-color: black;
  padding-vertical: 20px;
  justify-content: space-between;
  align-items: center;
  padding-horizontal: 20px;
  flex-direction: row;
`;

const IconWrapper = styled.View`
  border-color: black;
  border-width: 1px;
  width: 25px;
  height: 25px;
  justify-content: center;
  align-items: center;
`;

const IngredientsWrapper = styled.View`
  margin-top: 10px;
  margin-bottom: 7px;
  margin-horizontal: 60px;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;

const IngredientsList = styled.ScrollView`
  height: 200px;
  margin-horizontal: 40px;
  border-radius: 20px;
  background-color: white;
`;

const IngredientsItem = styled.View`
  padding-vertical: 15px;
  border-color: black;
  border-bottom-width: 1;
  padding-left: 20px;
`;

const NoProductsAdded = styled.View`
  align-items: center;
  margin-top: 10px;
`;
