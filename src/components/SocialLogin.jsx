import React from 'react';
import {Alert} from 'react-native';

import styled from 'styled-components';

const SocialItem = props => {
  const onPressUndefined = () => {
    Alert.alert('Funkcja dostępna wkrótce', 'Przepraszamy za utrudnienia', [
      {text: 'OK'},
    ]);
  };

  return (
    <SocialWrapper
      onPress={props.onPress === undefined ? onPressUndefined : props.onPress}
      style={{
        shadowOffset: {
          width: 0,
          height: 1,
        },
        elevation: 2,
      }}>
      <SocialImage source={props.img} />
    </SocialWrapper>
  );
};

export const SocialLogin = props => {
  return (
    <LoginBySocial style={props.style}>
      <SocialItem
        onPress={props.onPress}
        img={require('@images/icons/facebook.png')}
      />
      <SocialItem
        onPress={props.onPress}
        img={require('@images/icons/google.png')}
      />
      <SocialItem
        onPress={props.onPress}
        img={require('@images/icons/twitter.png')}
      />
    </LoginBySocial>
  );
};

const SocialWrapper = styled.TouchableOpacity`
  background-color: white;
  width: 50px;
  height: 50px;
  border-radius: 10px;
  margin-horizontal: 20px;
  justify-content: center;
  align-items: center;
  padding: 10px;
  shadow-color: #000;
  shadow-opacity: 0.2;
  shadow-radius: 1.41px;
`;

const SocialImage = styled.Image`
  width: 100%;
  height: 100%;
`;

const LoginBySocial = styled.View`
  flex-direction: row;
  justify-content: center;
`;
