import React from 'react';
import {TouchableOpacity, Text, Modal} from 'react-native';

import styled from 'styled-components';

import {vw, vh} from '@utils/sizes';

export const LoggingModal = props => {
  return (
    <Modal visible={props.visible} animationType="fade" transparent>
      <ModalWrapper>
        <ModalDisplay vw={vw()} vh={vh()}>
          <Text>{props.values}</Text>
          <TouchableOpacity onPress={props.setVisible}>
            <Text>close</Text>
          </TouchableOpacity>
        </ModalDisplay>
      </ModalWrapper>
    </Modal>
  );
};

const ModalWrapper = styled.View`
  flex: 1;
  justify-content: center;
  align-items: center;
`;

const ModalDisplay = styled.View`
  background-color: white;
  height: ${p => p.vh / 3 + 'px'};
  width: ${p => p.vw - 50 + 'px'};
  border-radius: 30px;
  justify-content: center;
  align-items: center;
`;
