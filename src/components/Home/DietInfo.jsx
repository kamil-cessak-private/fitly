import React, {useEffect, useState} from 'react';
import {View} from 'react-native';

import PercentageCircle from 'react-native-percentage-circle';
import styled from 'styled-components';

import {SectionHeader} from './SectionHeader';

import {vh} from '@utils/sizes';

export const DietInfo = () => {
  const [complete, setComplete] = useState(0);
  const [eaten, setEaten] = useState(1128);
  const [burned, setBurned] = useState(102);
  const [maxKcal, setMaxKcal] = useState(2000);
  const [kcalLeft, setKcalLeft] = useState(0);

  useEffect(() => {
    if (kcalLeft === 0) {
      let resultKcal = maxKcal - eaten + burned;
      setKcalLeft(resultKcal);

      if (complete === 0) {
        let resultComplete = (resultKcal / maxKcal) * 100;
        setComplete(resultComplete);
      }
    }
  }, []);

  const renderKcal = (color, data) => {
    return (
      <KcalWrapper color={color}>
        <KcalTypeTitle>{color === 'blue' ? 'Eaten' : 'Burned'}</KcalTypeTitle>
        <KcalImageWrapper>
          {color === 'blue' ? (
            <KcalImage source={require('@images/icons/water-drop.png')} />
          ) : (
            <KcalImage source={require('@images/icons/flame.png')} />
          )}
          <KcalCount>{data}</KcalCount>
          <KcalText>kcal</KcalText>
        </KcalImageWrapper>
      </KcalWrapper>
    );
  };

  const renderNutritions = (type, percentage, color, leftG, last) => {
    let leftComplete = 100 - percentage;
    leftComplete = leftComplete.toString() + '%';

    const complete = percentage.toString() + '%';
    return (
      <ItemWrapper>
        <ItemType>{type}</ItemType>
        <ProgressWrapper>
          <ProgressComplete color={color} complete={complete} />
          <ProgressIncomplete color={color} complete={leftComplete} />
        </ProgressWrapper>
        <ProgressText>{leftG + 'g '}left</ProgressText>
      </ItemWrapper>
    );
  };

  return (
    <DietWrapper>
      <SectionHeader
        title="Mediterranean diet"
        subtitle="Details"
        onPress={() => console.debug('details clicked')}
      />
      <DietInfoWrapper
        vh={vh()}
        style={{
          shadowOffset: {
            width: 2,
            height: 4,
          },
          elevation: 4,
        }}>
        <KcalContainer>
          <View>
            {renderKcal('blue', eaten)}
            {renderKcal('red', burned)}
          </View>
          <KcalCircleWrapper>
            <PercentageCircle
              radius={60}
              percent={complete}
              color={'#4f52dd'}
              borderWidth={10}>
              <PercentageLeft>{kcalLeft}</PercentageLeft>
              <PercentageText>kcal left</PercentageText>
            </PercentageCircle>
          </KcalCircleWrapper>
        </KcalContainer>
        <NutritionsWrapper>
          {renderNutritions('Carbs', 10, '#9eaff5', 100)}
          {renderNutritions('Protein', 30, '#dd7493', 70)}
          {renderNutritions('Fat', 70, '#f0c97e', 10, true)}
        </NutritionsWrapper>
      </DietInfoWrapper>
    </DietWrapper>
  );
};
const DietWrapper = styled.View`
  padding: 30px;
`;

const KcalWrapper = styled.View`
  border-left-width: 2px;
  border-left-color: ${p => (p.color === 'blue' ? '#ced3f1' : '#f0c6d4')};
  padding-left: 10px;
  margin-bottom: 20px;
`;

const KcalTypeTitle = styled.Text`
  color: #acaab7;
  font-weight: 500;
`;

const KcalImageWrapper = styled.View`
  flex-direction: row;
  align-items: center;
`;

const KcalImage = styled.Image`
  width: 25px;
  height: 25px;
  marginr-right: 7px;
`;

const KcalCount = styled.Text`
  color: #111;
  font-size: 30px;
  margin-right: 5px;
`;

const KcalText = styled.Text`
  color: #acaab7;
  align-self: flex-end;
  margin-bottom: 5px;
`;

const DietInfoWrapper = styled.View`
  shadow-color: #000;
  shadow-opacity: 0.2;
  shadow-radius: 1.41px;
  height: ${p => p.vh / 3};
  width: 100%;
  background-color: white;
  border-radius: 15px;
  border-top-right-radius: 75px;
  padding: 30px;
  justify-content: space-between;
`;

const KcalContainer = styled.View`
  flex-direction: row;
  align-items: center;
  justify-content: space-between;
  border-bottom-width: 1px;
  border-color: #eff0f5;
`;

const KcalCircleWrapper = styled.View`
  flex: 1;
  height: 100%;
  justify-content: center;
  align-items: flex-end;
`;

const NutritionsWrapper = styled.View`
  flex-direction: row;
  justify-content: space-between;
`;

const PercentageLeft = styled.Text`
  font-size: 30px;
  color: #4f4fbf;
  font-weight: 500;
`;

const PercentageText = styled.Text`
  font-size: 12px;
  color: #abaaaf;
`;

const ItemWrapper = styled.View`
  flex: 1;
  padding-right: ${p => !p.last && 30 + 'px'};
`;

const ItemType = styled.Text`
  color: #111;
  font-weight: 500;
`;

const ProgressWrapper = styled.View`
  flex-direction: row;
  width: 100%;
`;

const ProgressComplete = styled.View`
  width: ${p => p.complete};
  background-color: ${p => p.color};
  height: 4px;
  border-top-left-radius: 10px;
  border-bottom-left-radius: 10px;
`;

const ProgressIncomplete = styled.View`
  width: ${p => p.complete};
  background-color: ${p => p.color};
  opacity: 0.5;
  height: 4px;
  border-right: 10px;
`;

const ProgressText = styled.Text`
  color: #999px;
  font-size: 12px;
`;
