import React, {useEffect} from 'react';
import {View, Text} from 'react-native';

import MultiSlider from '@ptomasroos/react-native-multi-slider';
import styled from 'styled-components';

import Droplet from '@images/icons/droplet.svg';

export const CountWeeks = ({selected, setSelected, setTitle}) => {
  useEffect(() => {
    if (selected === '') {
      setSelected('1');
    }
    setTitle('How long you wish till you reach this goal?');
  }, []);

  const renderLabel = ({oneMarkerLeftPosition, oneMarkerValue}) => {
    let leftRepairValue = oneMarkerLeftPosition < 24 ? 4 : 8;

    return (
      <LabelWrapper
        oneMarkerLeftPosition={oneMarkerLeftPosition}
        leftRepairValue={leftRepairValue}>
        <Text style={{fontSize: 15}}>{oneMarkerValue}</Text>
      </LabelWrapper>
    );
  };

  const renderMarker = () => {
    return <Droplet width={50} height={100} fill="white" />;
  };

  return (
    <ScreenWrapper>
      <MultiSlider
        min={1}
        max={100}
        style={{}}
        enableLabel
        onValuesChangeFinish={v => setSelected(v[0])}
        selectedStyle={styles.slider}
        unselectedStyle={styles.slider}
        customMarker={renderMarker}
        customLabel={renderLabel}
      />
      <View style={{marginTop: 20}}>
        <Text style={{fontSize: 20}}>In weeks</Text>
      </View>
    </ScreenWrapper>
  );
};

const styles = {
  slider: {
    backgroundColor: '#1e80bf',
    height: 25,
    borderRadius: 20,
  },
};

const ScreenWrapper = styled.View`
  width: 100%;
  height: 50%;
  margin-top: 70px;
  padding-bottom: 25px;
  padding-horizontal: 50px;
  align-items: center;
`;

const LabelWrapper = styled.View`
  margin-bottom: 10px;
  left: ${p => p.oneMarkerLeftPosition - p.leftRepairValue + 'px'};
`;
