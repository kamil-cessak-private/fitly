import React from 'react';

import styled from 'styled-components';

import {vw, vh} from '@utils/sizes';

export const RoundedHeader = props => {
  return (
    <HeaderWrapper
      vw={vw()}
      vh={vh() / 5}
      style={{
        shadowOffset: {
          width: 0,
          height: 1,
        },
        elevation: 2,
      }}>
      <HeaderTitle size={props.size} weight={props.weight}>
        {props.title}
      </HeaderTitle>
      {props.desc && <HeaderDescription>{props.desc}</HeaderDescription>}
    </HeaderWrapper>
  );
};

const HeaderTitle = styled.Text`
  font-size: ${p => (p.size ? p.size : '50px')};
  color: #005b96;
  font-weight: ${p => (p.weight ? p.weight : '700')};
`;
const HeaderDescription = styled.Text`
  font-size: 15px;
  color: #005b96;
  font-weight: 400;
`;
const HeaderWrapper = styled.View`
  width: ${p => p.vw + 'px'};
  height: ${p => p.vh + 'px'};
  background-color: white;
  border-bottom-right-radius: 75px;
  justify-content: flex-end;
  padding-bottom: 25px;
  padding-left: 25px;
  shadow-color: #000;
  shadow-opacity: 0.2;
  shadow-radius: 1.41px;
`;
