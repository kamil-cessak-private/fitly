There is no re-render after updating state
    -Probably u have to copy you state and then modify it for e.g. :
        let temp = [...state];

Check if value is in array of objects:
    const found = arr.some(e => e.id === itemID);

Find index of element in array of objects:
    const idx = arr.findIndex(e => {return e.id === itemID});

Bitbucket error:
    Create app password:
        [+]https://bitbucket.org/account/settings/app-passwords/
    Reset local password: 
        [+]git config --global credential.helper osxkeychain
    Then try to git pull or something else and enter new app-password
